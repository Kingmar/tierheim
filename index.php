<?php

/**
 * This product is license-free and can be used and changed by anybody
 * without any warranty on its usage.
 *
 * If you have questions regarding its functionality or proposals for its
 * improvement, I encourage you to present them on the public GitHub.
 * You find the necessary information (and maybe more) in the product's
 * README.md in the root directory.
 *
 * Sincerely,
 * Ingmar Szmais
 */

declare(strict_types=1);

use Controller\DateHandler;
use Controller\LoginHandler;
use Controller\Repository\UserRepository;
use Controller\UserHandler;
use View\Base;
use View\Notification;

include 'autoload.php';

if (!isset($_POST['cmd'])) {
    Base::getInstance()->render();
} else {
    try {
        if (!isset($_POST['session']) || $_POST['cmd'] === 'logout') {
            if ($_POST['cmd'] === 'login') {
                LoginHandler::getInstance()->handleLogin();
            } else {
                LoginHandler::getInstance()->showLogin();
            }
        } else {
            setlocale(LC_ALL, 'de_DE@euro', 'de_DE', 'deu_deu');
            DateHandler::getInstance()->rollDates();
            global $user;
            if (str_starts_with($_POST['session'], 'admin')) {
                if (UserRepository::getInstance()->hasAdmin($_POST['session'])) {
                    UserRepository::getInstance()->deleteOldUsers();
                    $user = UserHandler::getInstance()->getAdminUser();
                }
            } else {
                $user = UserRepository::getInstance()->getUser($_POST['session']);
            }
            if ($user === null) {
                LoginHandler::getInstance()->showLogin();
            }
            switch ($_POST['cmd']) {
                case 'book' :
                    DateHandler::getInstance()->book();
                case 'showHelp' :
                    UserHandler::getInstance()->showHelp();
                case 'setSetting' :
                    if ($user->isAdmin()) {
                        UserHandler::getInstance()->setSetting();
                    }
                    LoginHandler::getInstance()->showLogin();
                case 'updateDate' :
                    if ($user->isAdmin()) {
                        DateHandler::getInstance()->updateDate();
                    }
                    LoginHandler::getInstance()->showLogin();
                case 'showBookings' :
                    if ($user->isAdmin()) {
                        DateHandler::getInstance()->showBookings();
                    }
                    LoginHandler::getInstance()->showLogin();
                default :
                    UserHandler::getInstance()->showUser();
            }
        }
    } catch (PDOException $e) {
        Notification::getInstance()->renderNotification($e->getMessage(), 'error');
    }
}