<?php

/**
 * This product is license-free and can be used and changed by anybody
 * without any warranty on its usage.
 *
 * If you have questions regarding its functionality or proposals for its
 * improvement, I encourage you to present them on the public GitHub.
 * You find the necessary information (and maybe more) in the product's
 * README.md in the root directory.
 *
 * Sincerely,
 * Ingmar Szmais
 */

declare(strict_types=1);

namespace View;

use JetBrains\PhpStorm\NoReturn;

class Base
{
    private static ?self $instance = null;
    protected string $html;

    public function __construct(protected ?string $key = null)
    {
        $this->html = $this->getHTML( $key ?? 'base');
    }

    protected function getHTML(string $name): string
    {
        return file_get_contents("View/templates/$name.html");
    }

    public static function getInstance(): self
    {
        self::$instance = self::$instance ?? new self();
        return self::$instance;
    }

    #[NoReturn] public function render(): void
    {
        echo $this->html;
        exit;
    }
}