<?php

/**
 * This product is license-free and can be used and changed by anybody
 * without any warranty on its usage.
 *
 * If you have questions regarding its functionality or proposals for its
 * improvement, I encourage you to present them on the public GitHub.
 * You find the necessary information (and maybe more) in the product's
 * README.md in the root directory.
 *
 * Sincerely,
 * Ingmar Szmais
 */

declare(strict_types=1);

namespace View;

use Controller\Repository\BaseRepository;
use JetBrains\PhpStorm\NoReturn;
use Model\Date as mDate;

class Admin extends Base
{
    private static ?self $instance = null;

    protected string $comment;
    protected string $dogs;
    protected string $special_dogs;

    public function __construct()
    {
        parent::__construct('admin');
        $this->comment = BaseRepository::getInstance()->getSetting('comment');
        $this->dogs = BaseRepository::getInstance()->getSetting('dogs');
        $this->special_dogs = BaseRepository::getInstance()->getSetting('special_dogs');
    }

    public static function getInstance(): self
    {
        self::$instance = self::$instance ?? new self();
        return self::$instance;
    }

    /**
     * @param mDate[] $dates
     */
    #[NoReturn] public function renderAdmin(array $dates): void
    {
        $this->html = str_replace('{DOGS}', $this->dogs, $this->html);
        $this->html = str_replace('{SPECIAL_DOGS}', $this->special_dogs, $this->html);
        $this->html = str_replace('{COMMENT}', $this->comment, $this->html);
        $this->html = str_replace('{DATES}', Date::getInstance()->withDates($dates)->getDates(), $this->html);

        $this->render();
    }
}