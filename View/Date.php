<?php

/**
 * This product is license-free and can be used and changed by anybody
 * without any warranty on its usage.
 *
 * If you have questions regarding its functionality or proposals for its
 * improvement, I encourage you to present them on the public GitHub.
 * You find the necessary information (and maybe more) in the product's
 * README.md in the root directory.
 *
 * Sincerely,
 * Ingmar Szmais
 */

declare(strict_types=1);

namespace View;

use JetBrains\PhpStorm\NoReturn;
use Model\Date as mDate;

class Date extends Base
{
    private static ?self $instance = null;

    protected array $dates;

    public static function getInstance(): self
    {
        self::$instance = self::$instance ?? new self();
        return self::$instance;
    }

    public function withDates(array $dates): self
    {
        $new = clone $this;
        $new->dates = $dates;
        return $new;
    }

    public function getDates(): string
    {
        $dates_html = '';
        foreach ($this->dates as $day) {
            $day_html = $this->getHTML('day');
            $block_html = '';
            foreach ($day as $block) {
                $block_html .= $this->getHTML('block');
                $block_html = str_replace('{TIMES}', $this->getBlockContent($block), $block_html);
            }
            $date = $day[array_key_first($day)][array_key_first($day[array_key_first($day)])];
            $day_html = str_replace('{DATE}', $date->format('l d.m'), $day_html);
            $day_html = str_replace('{BLOCKS}', $block_html, $day_html);
            $dates_html .= $day_html;
        }

        return $dates_html;
    }

    /**
     * @param mDate[] $dates
     */
    public function getBlockContent(array $dates): string
    {
        $html = '';
        global $user;
        foreach ($dates as $date) {
            $html .= $this->getHTML('time');
            switch ($date->getState()) {
                case mDate::BOOKED_OVER :
                    $state = 'over';
                    break;
                case mDate::BOOKED_FULL :
                    $state = 'full';
                    break;
                case mDate::BOOKED_FREE :
                    $state = 'free';
                    break;
                case mDate::ACTIVE :
                    $state = 'active';
                    break;
                case mDate::SKIPPED :
                    $state = 'skipped';
                    break;
                default;
                case mDate::BLOCKED :
                    $state = 'blocked';
                    break;
            }
            $html = str_replace('{STATE}', $state, $html);
            if ($user->isAdmin()) {
                $html = str_replace('{ACTION}', '✎', $html);
            } else {
                if ($date->getTraining()) {
                    $html = str_replace('{TRAINING}', 'training', $html);
                }
                $html = str_replace('{ACTION}', 'T', $html);
            }
            $html = str_replace('{TRAINING}', '', $html);
            $html = str_replace('{ID}', (string) $date->getId(), $html);
            $html = str_replace('{TIME}', $date->format('H:i'), $html);
            $html = str_replace('{COMMENT}', $date->getComment(), $html);
        }
        return $html;
    }

    #[NoReturn] public function renderBlock(): void
    {
        $this->html = $this->getBlockContent($this->dates);
        parent::render();
    }
}