{
    let user = document.querySelector('.user');
    let dates = user.querySelector('.dates');

    user.querySelector('#changeUser').addEventListener('click', () => {
        let data = new FormData();
        data.append('cmd', 'logout');
        action(document.querySelector('#content'), data).then();
    })
    let updateDate = element => {
        element.querySelectorAll('.dates .time').forEach(time => {
            time.querySelector('.action').addEventListener('click', event => {
                let data = new FormData();
                data.append('cmd', 'book');
                data.append('id', time.id);
                data.append('training', event.target.classList.contains('training') ? '0' : '1');
                let block = time.parentNode;
                action(block, data).then(() => {
                    updateDate(block);
                });
                event.stopPropagation();
            })
            time.addEventListener('click', () => {
                let data = new FormData();
                data.append('cmd', 'book');
                data.append('id', time.id);
                let block = time.parentNode;
                action(block, data).then(() => {
                    updateDate(block);
                })
            })
        })
    }
    updateDate(dates);
}