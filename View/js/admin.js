{
    let admin = document.querySelector('.admin');
    let controls = admin.querySelector('.controls');
    let dates = admin.querySelector('.dates');

    controls.querySelectorAll('input, textarea').forEach(element => {
        let save;
        element.addEventListener('input', () => {
            clearTimeout(save);
            save = setTimeout(() => {
                let data = new FormData();
                data.append('cmd', 'setSetting');
                data.append(element.id, element.value);
                action(document.querySelector('#content'), data).then();
            }, 300);
        })
    })

    let updateDate = element => {
        element.querySelectorAll('.time textarea[readonly]').forEach(comment => {
            comment.readOnly = false;
        })
        element.querySelectorAll('.dates .time').forEach(time => {
            time.addEventListener('click', () => {
                let data = new FormData();
                data.append('cmd', 'updateDate');
                data.append('id', time.id);
                let block = time.parentNode;
                action(block, data).then(() => {
                    updateDate(block);
                })
            })
            time.querySelector('.action').addEventListener('click', event => {
                dates.querySelectorAll('.time').forEach(i => {
                    i.classList.remove('edit');
                })
                dates.querySelector('#' + time.id).classList.add('edit');
                event.stopPropagation();
            })
            let comment = time.querySelector('textarea');
            comment.addEventListener('click', event => {
                event.stopPropagation();
                return false;
            })
            let saveComment;
            comment.addEventListener('input', () => {
                clearTimeout(saveComment);
                saveComment = setTimeout(() => {
                    let data = new FormData();
                    data.append('cmd', 'updateDate');
                    data.append('id', time.id);
                    data.append('comment', comment.value);
                    action(document.querySelector('#content'), data).then();
                }, 300);
            })
        })
    }
    updateDate(dates);
}