let action = (element, data) => {
    return new Promise((resolve, reject) => {
        let response = document.querySelector('#response');
        let xhr = new XMLHttpRequest();
        if (localStorage.getItem('session') !== null) {
            data.append('session', localStorage.getItem('session'));
        }
        xhr.open('POST', 'index.php')
        xhr.onload = () => {
            if (xhr.status === 200) {
                response.innerHTML = xhr.responseText;
                if (response.querySelector('session') !== null) {
                    localStorage.setItem('session', response.querySelector('session').innerHTML);
                    let cmd = new FormData();
                    cmd.append('cmd', 'show');
                    action(document.querySelector('#content'), cmd).then();
                    return false;
                }
                if (response.querySelector('title') !== null) {
                    document.querySelector('head title').innerHTML = response.querySelector('title').innerHTML;
                    response.querySelector('title').remove();
                }
                if (response.querySelector('overlay') !== null) {
                    document.querySelector('#overlay').innerHTML = response.querySelector('overlay').innerHTML;
                    response.querySelector('overlay').remove();
                }
                if (response.querySelector('notification') !== null) {
                    document.querySelector('#notification').classList = response.querySelector('notification').classList;
                    document.querySelector('#notification').innerHTML = response.querySelector('notification').innerHTML;
                    setTimeout(() => {
                        document.querySelector('#notification').innerHTML = '';
                        document.querySelector('#notification').classList = new DOMTokenList();
                    }, 5000);
                    response.querySelector('notification').remove();
                }
                if (response.innerHTML.trim() !== '') {
                    element.innerHTML = xhr.responseText;
                    element.querySelectorAll('script').forEach(js => {
                        let script = document.createElement('script');
                        script.src = js.src;
                        element.appendChild(script);
                        element.querySelector('script').remove();
                    })
                    response.innerHTML = '';
                }
                resolve();
            } else {
                console.error(xhr.status + ':' + xhr.responseText);
                reject();
            }
        };
        xhr.send(data);
    })
}

document.addEventListener('DOMContentLoaded', () => {
    let data = new FormData();
    data.append('cmd', 'show');
    action(document.querySelector('#content'), data).then();
    document.querySelector('#overlay').addEventListener('click', () => {
        document.querySelector('#overlay').innerHTML = '';
    })
})