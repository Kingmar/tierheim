{
    localStorage.clear();
    let login = document.querySelector('.login');
    let form = login.querySelector('#loginForm');

    form.querySelectorAll('.save').forEach(element => {
        element.addEventListener('click', () => {
            sendLogin();
        })
    })

    document.addEventListener("keypress", function (event) {
        if (event.key === "Enter") {
            sendLogin()
        }
    });

    let sendLogin = () => {
        let data = new FormData();
        data.append('cmd', 'login');
        if (form.classList.contains('user-login')) {
            data.append('id', login.querySelector('#id').value);
            data.append('name', login.querySelector('#name').value);
            data.append('certified', (login.querySelector('#certified').checked) ? '1' : '0');
            action(document.querySelector('#content'), data).then();
        }
        if (form.classList.contains('admin-login')) {
            data.append('password', login.querySelector('#password').value);
            action(document.querySelector('#content'), data).then();
        }
    };

    login.querySelectorAll('.select').forEach(element => {
        element.addEventListener('click', () => {
            login.querySelectorAll('.select').forEach(element => {
                element.classList.remove('active');
            })
            element.classList.add('active');
            form.classList.value = element.id;
        })
    })

    form.querySelectorAll('.checkbox').forEach(element => {
        element.addEventListener('click', () => {
            element.classList.toggle('checked');
            element.querySelector('input').checked = !element.querySelector('input').checked;
        })
    })
}