{
    let dates = document.querySelector('.dates')

    dates.querySelectorAll('.day > .date').forEach(element => {
        element.addEventListener('click', () => {
            let ids = [];
            element.parentNode.querySelectorAll('.time').forEach(time => {
                ids.push(time.id);
            })
            let data = new FormData();
            data.append('cmd', 'showBookings');
            data.append('ids', ids.join());
            action(document.querySelector('#content'), data).then();
        })
    })
}