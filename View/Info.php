<?php

/**
 * This product is license-free and can be used and changed by anybody
 * without any warranty on its usage.
 *
 * If you have questions regarding its functionality or proposals for its
 * improvement, I encourage you to present them on the public GitHub.
 * You find the necessary information (and maybe more) in the product's
 * README.md in the root directory.
 *
 * Sincerely,
 * Ingmar Szmais
 */

declare(strict_types=1);

namespace View;

use Controller\Repository\BaseRepository;
use JetBrains\PhpStorm\NoReturn;
use Model\Relation;
use Model\Date as mDate;

class Info extends Base
{
    private static ?self $instance = null;

    /**
     * @var Relation[][][]
     */
    protected array $relations;
    protected mDate $date;
    protected string $dogs;
    protected string $special_dogs;

    public function __construct()
    {
        parent::__construct('info');
        $this->dogs = BaseRepository::getInstance()->getSetting('dogs');
        $this->special_dogs = BaseRepository::getInstance()->getSetting('special_dogs');
    }

    public static function getInstance(): self
    {
        self::$instance = self::$instance ?? new self();
        return self::$instance;
    }

    public function withDate(mDate $date): self
    {
        $new = clone $this;
        $new->date = $date;
        return $new;
    }

    public function withRelations(array $relations): self
    {
        $new = clone $this;
        $new->relations = $relations;
        return $new;
    }

    #[NoReturn] public function renderInfo(): void
    {
        $this->html = str_replace('{DATE}', $this->date->format('l d.m'), $this->html);
        $block_html = '';
        foreach ($this->relations as $block) {
            $block_html .= $this->getHTML('info_block');
            $dogs = 0;
            $special_dogs = 0;
            $training = 0;
            $time_html = '';
            foreach ($block as $time) {
                if (count($time) > 0) {
                    $time_html .= $this->getHTML('info_time');
                    $dogs_names = [];
                    $special_dogs_names = [];
                    $training_names = [];
                    foreach ($time as $id => $relation) {
                        if ($id === 0) {
                            $time_html = str_replace('{TIME}', $relation->getDate()->format('H:i'), $time_html);
                        }
                        if ($relation->isTraining()) {
                            $training_names[] = $relation->getUser()->getName();
                            $training++;
                        } elseif ($relation->getUser()->getCertified()) {
                            $special_dogs_names[] = $relation->getUser()->getName();
                            $special_dogs++;
                        } else {
                            $dogs_names[] = $relation->getUser()->getName();
                            $dogs++;
                        }
                    }
                    $time_html = str_replace('{SPECIAL_NAMES}', implode('<br>', $special_dogs_names), $time_html);
                    $time_html = str_replace('{NAMES}', implode('<br>', $dogs_names), $time_html);
                    $time_html = str_replace('{TRAINING_NAMES}', implode('<br>', $training_names), $time_html);
                }
            }
            $block_html = str_replace('{SPECIAL_DOG_BOOKED}', (string) $special_dogs, $block_html);
            $block_html = str_replace('{SPECIAL_DOG_TOTAL}', $this->special_dogs, $block_html);
            $block_html = str_replace('{DOG_BOOKED}', (string) $dogs, $block_html);
            $block_html = str_replace('{DOG_TOTAL}', $this->dogs, $block_html);
            $block_html = str_replace('{TRAINING_DOG_BOOKED}', (string) $training, $block_html);
            $block_html = str_replace('{TIMES}', $time_html, $block_html);

        }
        $this->html = str_replace('{BLOCK}', $block_html, $this->html);
        parent::render();
    }
}