<?php

/**
 * This product is license-free and can be used and changed by anybody
 * without any warranty on its usage.
 *
 * If you have questions regarding its functionality or proposals for its
 * improvement, I encourage you to present them on the public GitHub.
 * You find the necessary information (and maybe more) in the product's
 * README.md in the root directory.
 *
 * Sincerely,
 * Ingmar Szmais
 */

declare(strict_types=1);

namespace View;

use JetBrains\PhpStorm\NoReturn;

class Notification extends Base
{
    private static ?self $instance = null;

    public static function getInstance(): self
    {
        self::$instance = self::$instance ?? new self();
        return self::$instance;
    }

    #[NoReturn] public function renderNotification(string $message, string $class = ''): void
    {
        $this->html = str_replace('{CLASS}', $class, $this->getHTML('notification'));
        $this->html = str_replace('{NOTIFICATION}', $message, $this->html);
        $this->render();
    }
}