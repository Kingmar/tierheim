# VetOrg

This project provides a UI for animal shelter to organize dog walks
for honorary walkers.

The project is published through its own [Gitlab Repository](https://gitlab.com/Kingmar/VetOrg.git)

## Requirements

For This to run you need:

[![Minimum PHP Version](https://img.shields.io/badge/PHP-8.2+-green.svg)](https://php.net/)

[![Mysql](https://img.shields.io/badge/Mysql-8.0+-green.svg)](https://mysql.com/)

and a modern Webserver (e.g. Nginx or Apache2).

## Installation

Download the project to your webserver.

1. Go to the target directory and execute the following line.
```
git clone https://gitlab.com/Kingmar/VetOrg.git
```
2. Open the `config.json` and fill it with your desired credentials _(see [Configuration](#configuration) for more info)_.
3. Now run the following command with your DB credentials
```
php install.php USERNAME PASSWORD
```
4. Your system will now be installed. If successfully you will see the line `Installation finished!` shortly after.

You can reset the whole system any time by redoing the Installation.
**Be aware that this removes all existing data!**

## Configuration

Here is an explanation to all parameter withing the `config.json`:
- **password** sets the admin password. This is used by the organizers of the page to open and close time spots (see [Usage](#usage) for more info).
- **weeks** sets the period of time in wich the timeslots rotate.
  - This defines in what scope times can be booked and have to be rebooked.
- **blocks** define time block where bookings can be made. A user can only make 1 booking per block.
  - **start** and **end** are the scope of said blocks. you can have as much block per one day as you desire.
- **database** sets credentials for you database. Contact you system administrator for more info.

## Usage

#### Organizer

As an organizers you can open and close booking durations and provide general info to the walkers.
- Click on **Organizer** and enter the Password set within the installation
- You are now on the organisation view of the side. Here you can do the following
  - Write a comment for all walker to see when they log in
  - Change the number of available dogs
  - Change the number of available special dogs
  - Click on a Dates header to see a list of all booked walkers
  - Open and close booking timeslots by clicking on them. Grey slots are closed, white slots are open.
   Stripped slots a closed until the next period (e.g. for holidays)

All changes are made on the side are saved automatically and immediately and respond in a message that says so or a visual change of the edited area.
There is no extra "save" button or else.

#### Walker

As a walker you can see dog capacities and can book time slots
- A walker can log in with its credentials.
  - **The ID-number has no further value of meaning, so you can give out any pattern you like.
  It just has to be unique for every walker to differ them.**
- On the page a walker can book time slots by clicking. A detailed explanation of the walkers options is provided by
  clicking the help section marked with an **?**.

The website does not safe any additional data that exceeds its functionality.
All walker data is safely encrypted and will be deleted after 3 month of inactivity.

## Technicals

#### Specification

There are no known limits to the Products usage.
However, be aware that this is made for small organisational units (< 200 users), therefore you may encounter visual
and technical problems when exceeding that limit.

#### Bugs

I'm pretty sure there a some, but I cant find any...
