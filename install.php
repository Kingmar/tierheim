<?php

/**
 * This product is license-free and can be used and changed by anybody
 * without any warranty on its usage.
 *
 * If you have questions regarding its functionality or proposals for its
 * improvement, I encourage you to present them on the public GitHub.
 * You find the necessary information (and maybe more) in the product's
 * README.md in the root directory.
 *
 * Sincerely,
 * Ingmar Szmais
 */

declare(strict_types=1);

if (php_sapi_name() !== "cli") {
    echo "Execute the install via commandline!\n";
    exit;
}
if ($argc < 3) {
    echo "Add the user and password of the database as parameters to the command!\n";
    exit;
}

echo "load config...\n";
$config = json_decode(file_get_contents('config.json'), true);
if (!str_starts_with($config['password'], '$argon2')) {
    echo "hash password...\n";
    $config['password'] = password_hash($config['password'], PASSWORD_ARGON2I);
    file_put_contents('config.json', json_encode($config));
}

$weeks = $config['weeks'];
$blocks = $config['blocks'];
$host = $config['database']['host'];
$user = $config['database']['user'];
$password = $config['database']['password'];
$schema = $config['database']['schema'];
echo "connect to DB...\n";
$connection = new PDO("mysql:host=$host", $argv[1], $argv[2]);
echo "create DB user...\n";
$connection->exec("DROP USER IF EXISTS '$user'@$host; CREATE USER '$user'@$host IDENTIFIED BY '$password';");
echo "create DB schema...\n";
$connection->exec("DROP SCHEMA IF EXISTS $schema; CREATE SCHEMA $schema;");
echo "set DB rights...\n";
$connection->exec("GRANT ALL PRIVILEGES ON $schema.* TO '$user'@$host;");
echo "create tables...\n";
$connection->exec("USE $schema;
    CREATE TABLE `date` (
        `id` int(20) UNSIGNED NOT NULL,
        `date` int(20) UNSIGNED NOT NULL,
        `block` tinyint(20) UNSIGNED NOT NULL,
        `state` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
        `comment` varchar(255) NOT NULL DEFAULT ''
    ) DEFAULT CHARSET=utf8;

    CREATE TABLE `relation` (
        `user_id` int(20) UNSIGNED NOT NULL,
        `date_id` int(20) UNSIGNED NOT NULL,
        `training` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
        `time` int(20) UNSIGNED NOT NULL
    ) DEFAULT CHARSET=utf8;

    CREATE TABLE `setting` (
        `id` varchar(255) NOT NULL,
        `value` varchar(255) NOT NULL
    ) DEFAULT CHARSET=utf8;

    CREATE TABLE `user` (
        `id` int(20) UNSIGNED NOT NULL,
        `name` varchar(255) NOT NULL,
        `certified` tinyint(1) NOT NULL,
        `session` varchar(255) NOT NULL,
        `last` int(20) UNSIGNED NOT NULL
    ) DEFAULT CHARSET=utf8;

    CREATE TABLE `admin` (
        `session` varchar(255) NOT NULL
    ) DEFAULT CHARSET=utf8;

    ALTER TABLE `date` ADD PRIMARY KEY (`id`);
    ALTER TABLE `user` ADD PRIMARY KEY (`id`);
    ALTER TABLE `setting` ADD PRIMARY KEY (`id`);
    ALTER TABLE `date` MODIFY `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT;
");

include 'autoload.php';
use Controller\Repository\BaseRepository;
use Controller\Repository\DateRepository;
use Model\Date;

echo "create dates...\n";
$date = new DateTime();
$date->setTime(0, 0);
$date->modify("+1 day");

$last = clone $date;
$last->modify("$weeks week");

$id = 0;
$block_id = 0;
while ($date < $last) {
    foreach ($blocks as $block) {
        $date->setTime($block['start'], 0);
        $end = clone $date;
        $end->setTime($block['end'], 30);
        while ($date <= $end) {
            DateRepository::getInstance()->createDate(new Date($id, $date, $block_id));
            if ($id === 0) {
                echo "created first Date for {$date->format('Y-m-d H:i:s')}\n";
            }
            $date->modify("+30 minute");
            $id++;
        }
        $block_id++;
    }
    $date->modify("+1 day");
}
echo "created last Date for {$date->format('Y-m-d H:i:s')}\n";
echo "$id Dates where created!\n";

echo "create settings...\n";
BaseRepository::getInstance()->addSetting('comment', '');
BaseRepository::getInstance()->addSetting('dogs', '');
BaseRepository::getInstance()->addSetting('SPECIAL_DOGs', '');

echo "Installation finished!\n\n";
