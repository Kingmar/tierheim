<?php

/**
 * This product is license-free and can be used and changed by anybody
 * without any warranty on its usage.
 *
 * If you have questions regarding its functionality or proposals for its
 * improvement, I encourage you to present them on the public GitHub.
 * You find the necessary information (and maybe more) in the product's
 * README.md in the root directory.
 *
 * Sincerely,
 * Ingmar Szmais
 */

declare(strict_types=1);

namespace Controller\Repository;

use DateTime;
use Model\Date;
use Model\Relation;
use Model\User;
use PDO;
use PDOException;
use PDOStatement;

class RelationRepository extends BaseRepository
{
    private static ?self $instance = null;
    private readonly DateTime $now;

    public function __construct()
    {
        parent::__construct();
        $this->now = new DateTime();
    }

    public function createRelation(User $user, Date $date, bool $training = false): void
    {
        $statement = $this->connection->prepare("INSERT INTO relation (user_id, date_id, training, time) VALUES (:user_id, :date_id, :training, :time);");
        $statement->bindValue(':user_id', $user->getId(), PDO::PARAM_INT);
        $statement->bindValue(':date_id', $date->getId(), PDO::PARAM_INT);
        $statement->bindValue(':training', ($training) ? 1 : 0, PDO::PARAM_INT);
        $statement->bindValue(':time', $this->now->getTimestamp(), PDO::PARAM_INT);
        if (!$statement->execute()) {
            throw new PDOException('Could not create relation of User ' . $user->getName() . ' with date ' . $date->format('H:i:s d.m.Y'));
        }
    }

    public function deleteRelation(User $user, Date $date): void
    {
        $statement = $this->connection->prepare("DELETE FROM relation WHERE user_id = :user_id AND date_id = :date_id;");
        $statement->bindValue(':user_id', $user->getId(), PDO::PARAM_INT);
        $statement->bindValue(':date_id', $date->getId(), PDO::PARAM_INT);
        if (!$statement->execute()) {
            throw new PDOException('Could not delete relation of User ' . $user->getName() . ' with date ' . $date->format('H:i:s d.m.Y'));
        }
    }

    public function deleteRelationsForDate(Date $date): void
    {
        $statement = $this->connection->prepare("DELETE FROM relation WHERE date_id = :date_id;");
        $statement->bindValue(':date_id', $date->getId(), PDO::PARAM_INT);
        if (!$statement->execute()) {
            throw new PDOException('Could not delete relation of date ' . $date->format('H:i:s d.m.Y'));
        }
    }

    /**
     * @return Relation[][]
     */
    public function getRelationsForBlock(int $block_id): array
    {
        $statement = $this->connection->prepare("SELECT * FROM user INNER JOIN relation ON user.id = relation.user_id INNER JOIN date ON relation.date_id = date.id WHERE date.block = :block_id ORDER BY date.date ASC, relation.time ASC");
        $statement->bindValue(':block_id', $block_id, PDO::PARAM_INT);
        return $this->fetchRelations($statement);
    }

    public static function getInstance(): self
    {
        self::$instance = self::$instance ?? new self();
        return self::$instance;
    }

    /**
     * @return Relation[][]
     */
    public function getActiveRelationsForBlock(int $block_id): array
    {
        $statement = $this->connection->prepare("SELECT * FROM user INNER JOIN relation ON user.id = relation.user_id INNER JOIN date ON relation.date_id = date.id WHERE date.block = :block_id AND date.state = :active_state ORDER BY date.date ASC, relation.time ASC");
        $statement->bindValue(':block_id', $block_id, PDO::PARAM_INT);
        $statement->bindValue(':active_state', Date::ACTIVE, PDO::PARAM_INT);
        return $this->fetchRelations($statement);
    }

    /**
     * @return Relation[][]
     */
    private function fetchRelations(PDOStatement $statement): array
    {
        $statement->execute();
        $relations = [];
        while ($row = $statement->fetch()) {
            $relations[$row['date_id']][] = new Relation(
                new Date(
                    $row['id'],
                    (new DateTime())->setTimestamp($row['date']),
                    $row['block'],
                    $row['state'],
                    $row['comment']
                ),
                $user ?? new User(
                        $row['user_id'],
                        UserRepository::getInstance()->decrypt($row['name']),
                        $row['certified'] === 1
                    ),
                $row['training'] === 1
            );
        }
        return $relations;
    }
}