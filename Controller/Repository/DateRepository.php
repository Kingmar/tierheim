<?php

/**
 * This product is license-free and can be used and changed by anybody
 * without any warranty on its usage.
 *
 * If you have questions regarding its functionality or proposals for its
 * improvement, I encourage you to present them on the public GitHub.
 * You find the necessary information (and maybe more) in the product's
 * README.md in the root directory.
 *
 * Sincerely,
 * Ingmar Szmais
 */

declare(strict_types=1);

namespace Controller\Repository;

use DateTime;
use Model\Date;
use PDO;
use PDOException;
use PDOStatement;

class DateRepository extends BaseRepository
{
    private static ?self $instance = null;
    private readonly DateTime $today;

    public function __construct()
    {
        parent::__construct();
        $today = new DateTime();
        $today->setTime(0, 0);
        $this->today = $today;
    }

    public static function getInstance(): self
    {
        self::$instance = self::$instance ?? new self();
        return self::$instance;
    }

    public function createDate(Date $date): int
    {
        $statement = $this->connection->prepare("INSERT INTO date (date, block) VALUES (:date, :block);");
        $statement->bindValue(':date', $date->getDate()->getTimestamp(), PDO::PARAM_INT);
        $statement->bindValue(':block', $date->getBlock(), PDO::PARAM_INT);
        if ($statement->execute()) {
            $statement = $this->connection->query("SELECT LAST_INSERT_ID();");
            return (int) $statement->fetch()['LAST_INSERT_ID()'];
        }
        throw new PDOException('Could not create Date ' . $date->format('H:i:s d.m.Y'));
    }

    public function getDate(int $id): Date
    {
        $statement = $this->connection->prepare("SELECT * FROM date WHERE id = :id");
        $statement->bindValue(':id', $id, PDO::PARAM_INT);
        $statement->execute();
        if ($row = $statement->fetch()) {
            return new Date(
                $row['id'],
                (new DateTime())->setTimestamp($row['date']),
                $row['block'],
                $row['state'],
                $row['comment']
            );
        }
        throw new PDOException('Could not find Date with id ' . $id);
    }

    public function updateDate(Date $date): void
    {
        $statement = $this->connection->prepare("UPDATE date SET date = :date, state = :state, comment = :comment WHERE id = :id");
        $statement->bindValue(':id', $date->getId(), PDO::PARAM_INT);
        $statement->bindValue(':date', $date->getDate()->getTimestamp(), PDO::PARAM_INT);
        $statement->bindValue(':state', $date->getState(), PDO::PARAM_INT);
        $statement->bindValue(':comment', $date->getComment());
        if (!$statement->execute()) {
            throw new PDOException('Could not update Date with id ' . $date->getId());
        }
    }

    /**
     * @return Date[]
     */
    public function getDatesForBlock(int $block_id): array
    {
        $statement = $this->connection->prepare("SELECT * FROM date WHERE block = :block ORDER BY date ASC");
        $statement->bindValue(':block', $block_id, PDO::PARAM_INT);
        return $this->fetchDates($statement);
    }

    /**
     * @return Date[]
     */
    public function getActiveDatesForBlock(int $block_id): array
    {
        $statement = $this->connection->prepare("SELECT * FROM date WHERE state = :state AND block = :block ORDER BY date ASC");
        $statement->bindValue(':state', DATE::ACTIVE, PDO::PARAM_INT);
        $statement->bindValue(':block', $block_id, PDO::PARAM_INT);
        return $this->fetchDates($statement);
    }

    /**
     * @return Date[]
     */
    public function getPastDates(): array
    {
        $statement = $this->connection->prepare("SELECT * FROM date WHERE date < :timestamp ORDER BY date ASC");
        $statement->bindValue(':timestamp', $this->today->getTimestamp(), PDO::PARAM_INT);
        return $this->fetchDates($statement);
    }

    /**
     * @return Date[]
     */
    public function getActiveDates(): array
    {
        $statement = $this->connection->prepare("SELECT * FROM date WHERE state = :state ORDER BY date ASC");
        $statement->bindValue(':state', DATE::ACTIVE, PDO::PARAM_INT);
        return $this->fetchDates($statement);
    }

    /**
     * @return Date[]
     */
    public function getAllDates(): array
    {
        $statement = $this->connection->prepare("SELECT * FROM date ORDER BY date ASC");
        return $this->fetchDates($statement);
    }

    /**
     * @return Date[]
     */
    private function fetchDates(PDOStatement $statement): array
    {
        $statement->execute();
        $dates = [];
        while ($row = $statement->fetch()) {
            $dates[$row['id']] = new Date(
                $row['id'],
                (new DateTime())->setTimestamp($row['date']),
                $row['block'],
                $row['state'],
                $row['comment']
            );
        }
        return $dates;
    }
}