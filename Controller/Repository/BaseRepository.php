<?php

/**
 * This product is license-free and can be used and changed by anybody
 * without any warranty on its usage.
 *
 * If you have questions regarding its functionality or proposals for its
 * improvement, I encourage you to present them on the public GitHub.
 * You find the necessary information (and maybe more) in the product's
 * README.md in the root directory.
 *
 * Sincerely,
 * Ingmar Szmais
 */

declare(strict_types=1);

namespace Controller\Repository;

use PDO;
use PDOException;

class BaseRepository
{
    private static ?self $instance = null;
    protected readonly PDO $connection;

    public function __construct()
    {
        $config = json_decode(file_get_contents('config.json'), true)['database'];
        $this->connection = new PDO("mysql:dbname=" . $config['schema'] . ";host=" . $config['host'], $config['user'], $config['password']);
    }

    public static function getInstance(): self
    {
        self::$instance = self::$instance ?? new self();
        return self::$instance;
    }

    public function getSetting(string $id): string
    {
        $statement = $this->connection->prepare("SELECT value FROM setting WHERE id = :id");
        $statement->bindValue(':id', $id);
        $statement->execute();
        if ($row = $statement->fetch()) {
            return $row['value'];
        }
        throw new PDOException('Could not find setting ' . $id);
    }

    public function setSetting(string $id, string $value): void
    {
        $statement = $this->connection->prepare("UPDATE setting SET value = :value WHERE id = :id");
        $statement->bindValue(':value', $value);
        $statement->bindValue(':id', $id);
        if (!$statement->execute()) {
            throw new PDOException('Could not update setting ' . $id);
        }
    }

    public function addSetting(string $id, string $value): void
    {
        $statement = $this->connection->prepare("INSERT INTO setting (id, value) VALUES (:id, :value)");
        $statement->bindValue(':id', $id);
        $statement->bindValue(':value', $value);
        if (!$statement->execute()) {
            throw new PDOException('Could not create setting ' . $id);
        }
    }
}