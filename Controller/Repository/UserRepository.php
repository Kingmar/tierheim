<?php

/**
 * This product is license-free and can be used and changed by anybody
 * without any warranty on its usage.
 *
 * If you have questions regarding its functionality or proposals for its
 * improvement, I encourage you to present them on the public GitHub.
 * You find the necessary information (and maybe more) in the product's
 * README.md in the root directory.
 *
 * Sincerely,
 * Ingmar Szmais
 */

declare(strict_types=1);

namespace Controller\Repository;

use DateTime;
use Model\User;
use PDO;
use PDOException;

class UserRepository extends BaseRepository
{
    private const CRYPT_ALGO = 'AES-256-OFB';
    private const CRYPT_PHRASE = 'm3adc2xhhhssf';
    private const CRYPT_IV = 'm3adc2xhhhssfx8,';
    private static ?self $instance = null;
    private readonly DateTime $now;

    public function __construct()
    {
        parent::__construct();
        $this->now = new DateTime();
    }

    public static function getInstance(): self
    {
        self::$instance = self::$instance ?? new self();
        return self::$instance;
    }

    public function createUser(User $user): string
    {
        $session = hash('sha256', (string) $user->getId());
        $statement = $this->connection->prepare("INSERT INTO user (id, name, certified, session, last) VALUES (:id, :name, :certified, :session, :last)");
        $statement->bindValue(':id', $user->getId(), PDO::PARAM_INT);
        $statement->bindValue(':name', $this->encrypt($user->getName()));
        $statement->bindValue(':certified', $user->getcertified(), PDO::PARAM_BOOL);
        $statement->bindValue(':session', $session);
        $statement->bindValue(':last', $this->now->getTimestamp());
        if ($statement->execute()) {
            return $session;
        }
        throw new PDOException('Could not create User with id ' . $user->getId() . 'and name' . $user->getName());
    }

    public function updateUser(User $user): string
    {
        $statement = $this->connection->prepare("UPDATE user SET name = :name, certified = :certified, last = :last WHERE id = :id");
        $statement->bindValue(':name', $this->encrypt($user->getName()));
        $statement->bindValue(':certified', $user->getCertified(), PDO::PARAM_BOOL);
        $statement->bindValue(':id', $user->getId(), PDO::PARAM_INT);
        $statement->bindValue(':last', $this->now->getTimestamp());
        if ($statement->execute()) {
            $statement = $this->connection->prepare("SELECT session FROM user WHERE id = :id");
            $statement->bindValue(':id', $user->getId(), PDO::PARAM_INT);
            $statement->execute();
            if ($row = $statement->fetch()) {
                return $row['session'];
            }
        }
        throw new PDOException('Could not update User with id ' . $user->getId());
    }

    public function hasUser(User $user): bool
    {
        $statement = $this->connection->prepare("SELECT * FROM user WHERE id = :id");
        $statement->bindValue(':id', $user->getId(), PDO::PARAM_INT);
        $statement->execute();
        if ($statement->fetch()) {
            return true;
        }
        return false;
    }

    public function getUser($session): ?User
    {
        $statement = $this->connection->prepare("SELECT * FROM user WHERE session = :session");
        $statement->bindValue(':session', $session);
        $statement->execute();
        while ($row = $statement->fetch()) {
            $user = new User(
                (int) $row['id'],
                $this->decrypt($row['name']),
                $row['certified'] === 1
            );
            $statement = $this->connection->prepare("UPDATE user SET last = :last WHERE id = :id");
            $statement->bindValue(':id', $user->getId(), PDO::PARAM_INT);
            $statement->bindValue(':last', $this->now->getTimestamp());
            if ($statement->execute()) {
                return $user;
            }
        }
        return null;
    }

    public function createAdmin(): string
    {
        $session = 'admin' . hash('sha256', (string) rand(0, 1000000));
        $statement = $this->connection->prepare("INSERT INTO admin (session) VALUES (:session)");
        $statement->bindValue(':session', $session);
        if ($statement->execute()) {
            return $session;
        }
        throw new PDOException('Could not create Admin');
    }

    public function hasAdmin(string $session): bool
    {
        $statement = $this->connection->prepare("SELECT * FROM admin WHERE session = :session");
        $statement->bindValue(':session', $session);
        $statement->execute();
        if ($statement->fetch()) {
            return true;
        }
        return false;
    }

    public function deleteOldUsers(): void
    {
        $deadline = clone $this->now;
        $deadline->modify("-1 month");
        $statement = $this->connection->prepare("DELETE FROM user WHERE last < :last");
        $statement->bindValue(':last', $deadline->getTimestamp(), PDO::PARAM_INT);
        if (!$statement->execute()) {
            throw new PDOException('Could not delete old User');
        }
    }

    private function encrypt($string): string
    {
        return openssl_encrypt($string, self::CRYPT_ALGO, self::CRYPT_PHRASE, 0, self::CRYPT_IV);
    }

    public function decrypt($string): string
    {
        return openssl_decrypt($string, self::CRYPT_ALGO, self::CRYPT_PHRASE, 0, self::CRYPT_IV);
    }
}