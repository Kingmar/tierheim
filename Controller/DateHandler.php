<?php

/**
 * This product is license-free and can be used and changed by anybody
 * without any warranty on its usage.
 *
 * If you have questions regarding its functionality or proposals for its
 * improvement, I encourage you to present them on the public GitHub.
 * You find the necessary information (and maybe more) in the product's
 * README.md in the root directory.
 *
 * Sincerely,
 * Ingmar Szmais
 */

declare(strict_types=1);

namespace Controller;

use Controller\Repository\BaseRepository;
use Controller\Repository\DateRepository;
use Controller\Repository\RelationRepository;
use DateTime;
use JetBrains\PhpStorm\NoReturn;
use Model\Date;
use Model\User;
use View\Info;
use View\Notification;
use View\Date as vDate;

class DateHandler
{
    private static ?self $instance = null;

    protected readonly int $scope;
    protected readonly int $dogs;
    protected readonly int $special_dogs;
    protected readonly DateTime $deadline;

    public function __construct()
    {
        $deadline = new DateTime();
        $deadline->modify("+2 hour");
        $this->deadline = $deadline;
        $config = json_decode(file_get_contents('config.json'), true);
        $this->dogs = (int)BaseRepository::getInstance()->getSetting('dogs');
        $this->special_dogs = (int)BaseRepository::getInstance()->getSetting('special_dogs');
        $this->scope = $config['weeks'];
    }

    public static function getInstance(): self
    {
        self::$instance = self::$instance ?? new self();
        return self::$instance;
    }

    public function rollDates(): void
    {
        $past_dates = DateRepository::getInstance()->getPastDates();

        foreach ($past_dates as $date) {
            RelationRepository::getInstance()->deleteRelationsForDate($date);
            $new_date = new Date(
                $date->getId(),
                $date->getDate()->modify("+$this->scope weeks"),
                $date->getBlock(),
                ($date->getState() === Date::SKIPPED) ? Date::ACTIVE : $date->getState()
            );
            DateRepository::getInstance()->updateDate($new_date);
        }
    }

    #[NoReturn] public function updateDate(): void
    {
        $repo = DateRepository::getInstance();
        $date = $repo->getDate((int)str_replace('time_', '', $_POST['id']));
        if (isset($_POST['comment'])) {
            $date->setComment($_POST['comment']);
            $repo->updateDate($date);
            Notification::getInstance()->renderNotification('Changes Saved!', 'success');
        } else {
            $date->setState(($date->getState() + 2) % 3);
            $repo->updateDate($date);
            vDate::getInstance()->withDates($repo->getDatesForBlock($date->getBlock()))->renderBlock();
        }
    }

    public function orderDates(User $user): array
    {
        $new_order = [];
        if (!$user->isAdmin()) {
            $dates = DateRepository::getInstance()->getActiveDates();
        } else {
            $dates = DateRepository::getInstance()->getAllDates();
        }
        foreach ($dates as $date) {
            $new_order[$date->getDate()->format('dmY')][$date->getBlock()][$date->getId()] = $date;
        }
        if (!$user->isAdmin()) {
            foreach ($new_order as $day) {
                foreach ($day as $id => $block) {
                    $this->calculateState($id, $block);
                }
            }
        }
        return $new_order;
    }

    /**
     * @param Date[] $dates
     */
    protected function calculateState(int $block, array &$dates): void
    {
        global $user;
        $special = $this->special_dogs;
        $normal = $this->dogs;
        foreach (RelationRepository::getInstance()->getActiveRelationsForBlock($block) as $date) {

            $own = false;
            foreach ($date as $relation) {
                if (!$relation->isTraining()) {
                    if ($relation->getUser()->getCertified()) {
                        $special--;
                    } else {
                        $normal--;
                    }
                }
                if ($relation->getUser() == $user) {
                    $own = $relation;
                }
            }
            if ($own !== false && key_exists($own->getDate()->getId(), $dates)) {
                if ($own->isTraining()) {
                    $dates[$own->getDate()->getId()]->setTraining(true);
                }
                if ($user->getCertified()) {
                    $cap = max(0, $normal) + $special;
                } else {
                    $cap = min($special, 0) + $normal;
                }
                if ($cap <= -3) {
                    $dates[$own->getDate()->getId()]->setState(Date::BOOKED_OVER);
                } elseif ($cap <= 0) {
                    $dates[$own->getDate()->getId()]->setState(Date::BOOKED_FULL);
                } else {
                    $dates[$own->getDate()->getId()]->setState(Date::BOOKED_FREE);
                }
            }
        }
        foreach ($dates as $date) {
            if ($date->getState() === Date::ACTIVE && $date->getDate() < $this->deadline) {
                $date->setState(Date::BLOCKED);
            }
        }
    }

    #[NoReturn] public function book(): void
    {
        global $user;
        $repo = RelationRepository::getInstance();
        $date = DateRepository::getInstance()->getDate((int)str_replace('time_', '', $_POST['id']));
        if ($date->getDate() > $this->deadline) {
            $old = null;
            foreach ($repo->getRelationsForBlock($date->getBlock()) as $dates) {
                foreach ($dates as $relation) {
                    if ($relation->getUser() == $user) {
                        $old = $relation->getDate();
                        if ($old->getDate() > $this->deadline) {
                            $repo->deleteRelation($user, $old);
                            if ($old->getDate() != $date->getDate() || isset($_POST['training'])) {
                                $old = null;
                            }
                        }
                    }
                }
            }
            if ($old === null) {
                $repo->createRelation($user, $date, ($_POST['training'] ?? '0') === '1');
            }
        }
        $dates = DateRepository::getInstance()->getActiveDatesForBlock($date->getBlock());
        $this->calculateState($date->getBlock(), $dates);
        vDate::getInstance()->withDates($dates)->renderBlock();
    }

    #[NoReturn] public function showBookings(): void
    {
        if (isset($_POST['ids'])) {
            $ids = explode(',', str_replace('time_', '', $_POST['ids']));
            $dates = [];
            $date = null;
            foreach ($ids as $id) {
                $date = DateRepository::getInstance()->getDate((int)$id);
                $dates[$date->getBlock()][$date->getId()] = [];
            }
            foreach ($dates as $block => $times) {
                $relations = RelationRepository::getInstance()->getActiveRelationsForBlock($block);
                foreach ($relations as $date_id => $relation) {
                    $dates[$block][$date_id] = $relation;
                }
            }
            Info::getInstance()->withDate($date)->withRelations($dates)->renderInfo();
        } else {
            Notification::getInstance()->renderNotification('Could not show bookings!', 'error');
        }
    }
}
