<?php

/**
 * This product is license-free and can be used and changed by anybody
 * without any warranty on its usage.
 *
 * If you have questions regarding its functionality or proposals for its
 * improvement, I encourage you to present them on the public GitHub.
 * You find the necessary information (and maybe more) in the product's
 * README.md in the root directory.
 *
 * Sincerely,
 * Ingmar Szmais
 */

declare(strict_types=1);

namespace Controller;

use Controller\Repository\BaseRepository;
use JetBrains\PhpStorm\NoReturn;
use Model\User;
use View\Admin;
use View\Help;
use View\Notification;
use View\User as vUser;

class UserHandler
{
    private static ?self $instance = null;

    public function getAdminUser(): User
    {
        return new User(0, '', false, true);
    }

    #[NoReturn] public function showUser(): void
    {
        global $user;

        if ($user->isAdmin()) {
            Admin::getInstance()->renderAdmin(DateHandler::getInstance()->orderDates($user));
        } else {
            vUser::getInstance()->renderUser(DateHandler::getInstance()->orderDates($user));
        }
    }

    public static function getInstance(): self
    {
        self::$instance = self::$instance ?? new self();
        return self::$instance;
    }

    #[NoReturn] public function showHelp(): void
    {
        Help::getInstance()->renderHelp();
    }

    #[NoReturn] public function setSetting(): void
    {
        unset($_POST['session']);
        unset($_POST['cmd']);
        if (count($_POST) === 1) {
            foreach ($_POST as $key => $value) {
                BaseRepository::getInstance()->setSetting($key, $value);
                Notification::getInstance()->renderNotification('Changes saved!', 'success');
            }
        }
        Notification::getInstance()->renderNotification('Could not save changes!', 'error');
    }
}