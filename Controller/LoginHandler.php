<?php

/**
 * This product is license-free and can be used and changed by anybody
 * without any warranty on its usage.
 *
 * If you have questions regarding its functionality or proposals for its
 * improvement, I encourage you to present them on the public GitHub.
 * You find the necessary information (and maybe more) in the product's
 * README.md in the root directory.
 *
 * Sincerely,
 * Ingmar Szmais
 */

declare(strict_types=1);

namespace Controller;

use Controller\Repository\UserRepository;
use JetBrains\PhpStorm\NoReturn;
use Model\User;
use View\Login;
use View\Notification;
use View\Session;

class LoginHandler
{
    private static ?self $instance = null;

    private readonly string $password_hash;

    public function __construct()
    {
        $config = json_decode(file_get_contents('config.json'), true);
        $this->password_hash = $config['password'];
    }

    #[NoReturn] public function handleLogin(): void
    {
        global $user;
        if (isset($_POST['password'])) {
            if (
                preg_match('/^[A-z0-9]+$/', $_POST['password']) === 1 &&
                password_verify($_POST['password'], $this->password_hash)
            ) {
                $session = UserRepository::getInstance()->createAdmin();
                $user = UserHandler::getInstance()->getAdminUser();
            } else {
                Notification::getInstance()->renderNotification('Invalid password!', 'error');
            }
        } else {
            if (
                $_POST['id'] !== '' &&
                is_numeric($_POST['id']) &&
                $_POST['name'] !== '' &&
                preg_match('/^[A-z0-9äöüÄÜÖß @.\-+"\'#´*~]+$/', $_POST['name']) === 1 &&
                ($_POST['certified'] === '1' || $_POST['certified'] === '0')
            ) {
                $user = new User((int) $_POST['id'], $_POST['name'], ($_POST['certified'] === '1'));
                $repo = UserRepository::getInstance();
                if ($repo->hasUser($user)) {
                    $session = $repo->updateUser($user);
                } else {
                    $session = $repo->createUser($user);
                }
            } else {
                Notification::getInstance()->renderNotification('Invalid input!', 'error');
            }
        }
        if (isset($session)) {
            Session::getInstance()->renderSession($session);
        } else {
            $this->showLogin();
        }
    }

    public static function getInstance(): self
    {
        self::$instance = self::$instance ?? new self();
        return self::$instance;
    }

    #[NoReturn] public function showLogin(): void
    {
        Login::getInstance()->renderLogin();
    }
}