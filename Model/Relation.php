<?php

/**
 * This product is license-free and can be used and changed by anybody
 * without any warranty on its usage.
 *
 * If you have questions regarding its functionality or proposals for its
 * improvement, I encourage you to present them on the public GitHub.
 * You find the necessary information (and maybe more) in the product's
 * README.md in the root directory.
 *
 * Sincerely,
 * Ingmar Szmais
 */

declare(strict_types=1);

namespace Model;

class Relation
{
    public function __construct(
        protected readonly Date $date,
        protected readonly User $user,
        protected bool $training = false
    ) {}

    public function isTraining(): bool
    {
        return $this->training;
    }

    public function getDate(): Date
    {
        return $this->date;
    }

    public function getUser(): User
    {
        return $this->user;
    }
}