<?php

/**
 * This product is license-free and can be used and changed by anybody
 * without any warranty on its usage.
 *
 * If you have questions regarding its functionality or proposals for its
 * improvement, I encourage you to present them on the public GitHub.
 * You find the necessary information (and maybe more) in the product's
 * README.md in the root directory.
 *
 * Sincerely,
 * Ingmar Szmais
 */

declare(strict_types=1);

namespace Model;

use DateTime;

class Date
{
    const BLOCKED = 0;
    const SKIPPED = 1;
    const ACTIVE = 2;
    const BOOKED_FREE = 3;
    const BOOKED_FULL = 4;
    const BOOKED_OVER = 5;

    public function __construct(
        protected readonly int $id,
        protected readonly DateTime $date,
        protected int $block,
        protected int $state = self::BLOCKED,
        protected string $comment = '',
        protected bool $training = false
    ) {}

    public function getId(): int
    {
        return $this->id;
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function getBlock(): int
    {
        return $this->block;
    }

    public function getState(): int
    {
        return $this->state;
    }

    public function setState(int $state): void
    {
        if ($state >= 0 && $state <= 6) {
            $this->state = $state;
        }
    }

    public function getComment(): string
    {
        return $this->comment;
    }

    public function setComment(string $comment): void
    {
        $this->comment = $comment;
    }

    public function getTraining(): bool
    {
        return $this->training;
    }

    public function setTraining(bool $training): void
    {
        $this->training = $training;
    }

    public function format(string $format): string
    {
        return $this->date->format($format);
    }
}