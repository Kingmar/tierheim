<?php

/**
 * This product is license-free and can be used and changed by anybody
 * without any warranty on its usage.
 *
 * If you have questions regarding its functionality or proposals for its
 * improvement, I encourage you to present them on the public GitHub.
 * You find the necessary information (and maybe more) in the product's
 * README.md in the root directory.
 *
 * Sincerely,
 * Ingmar Szmais
 */

declare(strict_types=1);

namespace Model;

class User
{
    public function __construct(
        protected readonly int $id,
        protected string $name,
        protected bool $certified,
        protected bool $admin = false
    ) {}

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getCertified(): bool
    {
        return $this->certified;
    }

    public function isAdmin(): bool
    {
        return $this->admin;
    }
}